package loginservice;

public interface IPassword {
	void setPasswordChanged(boolean value);
	boolean passwordChanges(String candidate);
	boolean passwordPrevious24(String candidate);
	boolean passwordGreaterThan24(String candidate);
}
