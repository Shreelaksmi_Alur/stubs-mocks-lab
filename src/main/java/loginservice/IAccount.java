package loginservice;

public interface IAccount {
	void setLoggedIn(boolean value);
	boolean passwordMatches(String candidate);
	boolean setRevoked(boolean value);
	boolean isLoggedIn();
	boolean isRevoked();
	boolean passwordExpired(String candidate);
	boolean passwordChanged(String candidate);
	boolean passwordTemporary(String candidate);
	
}
