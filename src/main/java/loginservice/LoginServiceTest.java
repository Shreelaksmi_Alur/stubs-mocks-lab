package loginservice;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import org.junit.Before;
import org.junit.Test;

public class LoginServiceTest {

	   private IAccount account;
	   private IAccountRepository accountRepository;
	   private LoginService service;
	 
	   @Before
	   public void init() {
	      account = mock(IAccount.class);
	      accountRepository = mock(IAccountRepository.class);
	      when(accountRepository.find(anyString())).thenReturn(account);
	      service = new LoginService(accountRepository);
	   }
	 
	   private void willPasswordMatch(boolean value) {
	      when(account.passwordMatches(anyString())).thenReturn(value);
	   }
	   
	   private void isPasswordExpired(boolean value){
		   when(account.passwordExpired(anyString())).thenReturn(value);
	   }
	   
	   private void isPasswordChanged(boolean value){
		   when(account.passwordChanged(anyString())).thenReturn(value);
	   }
	   
	   private void isPasswordTemporary(boolean value){
		   when(account.passwordTemporary(anyString())).thenReturn(value);
	   }
	   
	   @Test
	   public void itShouldSetAccountToLoggedInWhenPasswordMatches() {
	      willPasswordMatch(true);
	      service.login("brett", "password");
	      verify(account, times(1)).setLoggedIn(true);
	   }
	   
	   @Test
	   public void itShouldSetAccountToRevokedAfterThreeFailedLoginAttempts() {
		   willPasswordMatch(false);
		   
		      for (int i = 0; i < 3; ++i)
		         service.login("brett", "password");
		 
		      verify(account, times(1)).setRevoked(true);
	   }
	   
	   @Test
	   public void itShouldNotSetAccountLoggedInIfPasswordDoesNotMatch() {
		   willPasswordMatch(false);
		   service.login("brett", "password");
		   verify(account, never()).setLoggedIn(true);
	   }
	 
	   @Test
	   public void itShouldNotRevokeSecondAccountAfterTwoFailedAttemptsFirstAccount() {
		   willPasswordMatch(false);
		   
		   IAccount secondAccount = mock(IAccount.class);
		   when(secondAccount.passwordMatches(anyString())).thenReturn(false);
		   when(accountRepository.find("schuchert")).thenReturn(secondAccount);

		   service.login("brett", "password");
		   service.login("brett", "password");
		   service.login("schuchert", "password");
		 
		   verify(secondAccount, never()).setRevoked(true);
	   }
	   
	   @Test(expected = AccountLoginLimitReachedException.class)
	   public void itShouldNowAllowConcurrentLogins() {
	      willPasswordMatch(true);
	      when(account.isLoggedIn()).thenReturn(true);
	      service.login("brett", "password");
	   }
	   
	   @Test(expected = AccountNotFoundException.class)
	   public void ItShouldThrowExceptionIfAccountNotFound() {
	      when(accountRepository.find("schuchert")).thenReturn(null);
	      service.login("schuchert", "password");
	   }
	   
	   @Test(expected = AccountRevokedException.class)
	   public void ItShouldNotBePossibleToLogIntoRevokedAccount() {
	      willPasswordMatch(true);
	      when(account.isRevoked()).thenReturn(true);
	      service.login("brett", "password");
	   }
	   
	   // Test added to verify: Cannot Login to Account with Expired Password
	   @Test
	   public void CannotLoginIfPasswordIsExpired() {
	      isPasswordExpired(true);
	      service.login("brett", "password");
		  verify(account, never()).setLoggedIn(true);
	   }
	   
	   // Test added to verify: Can Login to Account with Expired Password After Changing the Password
	   @Test
	   public void CanLoginIfPasswordIsExpiredAfterPasswordIsChanged() {
	      isPasswordExpired(true);
	      isPasswordChanged(true);
	      service.login("brett", "password");
		  verify(account, times(1)).setLoggedIn(true);
	   }
	   
	   // Test added to verify: Cannot Login to Account with Temporary Password
	   @Test
	   public void CannotLoginIfPasswordIsTemporary() {
	      isPasswordTemporary(true);
	      service.login("brett", "password");
		  verify(account, never()).setLoggedIn(true);
	   }
	   
	   // Test added to verify: Can Login to Account with Temporary Password After Changing Password
	   @Test
	   public void CanLoginIfPasswordIsTemporaryAfterPasswordIsChanged() {
	      isPasswordTemporary(true);
	      isPasswordChanged(true);
	      service.login("brett", "password");
		  verify(account, times(1)).setLoggedIn(true);
	   }
	   
	  
}
