package loginservice;

public class PasswordService {
	private final IPasswordRepository passwordRepository;

	public PasswordService(IPasswordRepository passwordRepository) {
		this.passwordRepository = passwordRepository;
	  }
	
	void changePassword(String oldPassword, String newPassword){
		IPassword password = passwordRepository.find(oldPassword);
		
		if(password.passwordGreaterThan24(newPassword)){
			password.setPasswordChanged(true);
		}
	}
}
