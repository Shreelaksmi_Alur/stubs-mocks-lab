package loginservice;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;

public class PasswordServiceTest {
	
	private IPassword password;
	private IPasswordRepository passwordRepository;
	private PasswordService passwordService;
	
	@Before
	   public void init() {
	      password = mock(IPassword.class);
	      passwordRepository = mock(IPasswordRepository.class);
	      when(passwordRepository.find(anyString())).thenReturn(password);
	      passwordService = new PasswordService(passwordRepository);
	   }
	
	 private void isPasswordAmongPrevious24Passwords(boolean value){
		   when(password.passwordChanges(anyString())).thenReturn(value);
	 }
	 
	 private void isPasswordGreaterThanPrevious24Passwords(boolean value){
		   when(password.passwordGreaterThan24(anyString())).thenReturn(value);
	 }
	 
	   // Test added to verify: Cannot Change Password to any of Previous 24 passwords
	   @Test
	   public void CannotChangePasswordIfAmongPrevious24Passwords() {
		  isPasswordAmongPrevious24Passwords(true);
		  passwordService.changePassword("old","new");
		  verify(password, never()).setPasswordChanged(true);
	   }
	   
	   // Test added to verify: Can Change Password to Previous password if > 24 Changes from last use
	   @Test
	   public void CanChangePasswordIfGreaterThanPrevious24Passwords() {
		  isPasswordGreaterThanPrevious24Passwords(true);
		  passwordService.changePassword("old","new");
		  verify(password, times(1)).setPasswordChanged(true);
	   }
}
