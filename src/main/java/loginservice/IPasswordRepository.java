package loginservice;

public interface IPasswordRepository {
	IPassword find(String passwordId);
}
